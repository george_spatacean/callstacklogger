#pragma once
#include <string>
#include <vector>
#include <map>
#include <algorithm>

class LogSectionRepository
{
	typedef std::map<std::string, int>::iterator LOGGING_SECTION_ITERATOR;
public:
	static LogSectionRepository& Instance()
	{
		static LogSectionRepository instance;
		return instance;
	}

	void AddTargetSection(const std::string& NewSection)
	{
		LOGGING_SECTION_ITERATOR target = _loggingSectionsMap.find(NewSection);
		if (target != _loggingSectionsMap.end())
		{
			//area already present, just increase counter;
			(target->second)++;
		}
		else
		{
			_loggingSectionsMap[NewSection] = 1;
		}
	}

	void RemoveTargetSection(const std::string& TargetSection)
	{
		LOGGING_SECTION_ITERATOR target = _loggingSectionsMap.find(TargetSection);
		if (target != _loggingSectionsMap.end())
		{
			if (target->second > 0)
			{
				(target->second)--;
			}
		}
	}

	bool IsActive(const std::string& TargetSection)
	{
		LOGGING_SECTION_ITERATOR target = _loggingSectionsMap.find(TargetSection);
		if (target != _loggingSectionsMap.end())
		{
			return target->second > 0;
		}
		return false;
	}
private:
	LogSectionRepository() {};
	std::map<std::string, int> _loggingSectionsMap;
};

class LogSectionTrigger
{
public:
	LogSectionTrigger(LogSectionRepository& LogTracker, const std::string& Area) :
		_logTracker(LogTracker),
		_Area(Area)
	{
		_logTracker.AddTargetSection(_Area);
	}

	~LogSectionTrigger()
	{
		_logTracker.RemoveTargetSection(_Area);
	}

private:
	LogSectionRepository& _logTracker;
	std::string _Area;
};

class ISectionLogging
{
public:
	ISectionLogging(std::string tag) :_tag(tag) {};
	virtual ~ISectionLogging() {};
	void LogThis(const std::string& Message = "Defaulted")
	{
		if (IsSectionActive())
		{
			CustomLogging(Message);
		}
	}

protected:
	std::string _tag;
	virtual void CustomLogging(const std::string& Message)
	{
		std::cout << Message << std::endl;
	}

private:
	bool IsSectionActive()
	{
		return LogSectionRepository::Instance().IsActive(_tag);
	}
};

class ThisSectionLogging : public ISectionLogging
{
public:
	ThisSectionLogging(std::string tag) :
		ISectionLogging(tag) {};
protected:
	void CustomLogging(const std::string& Message)
	{
		std::cout << "This is ThisSection Custom " << Message << std::endl;
	}
};

class ThatSectionLogging : public ISectionLogging
{
public:
	ThatSectionLogging(std::string tag) :
		ISectionLogging(tag) {};
};