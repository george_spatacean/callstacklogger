#include <iostream>
#include "LogTracker.h"

void f6()
{
	ThatSectionLogging section("F2_PATH");
	ThatSectionLogging critical("TEST");
	std::cout << "In f6()" << std::endl;
	section.LogThis();
	critical.LogThis();
}

void f5()
{
	ThatSectionLogging specific("F2_PATH");
	std::cout << "In f5()" << std::endl;
	specific.LogThis();
	specific.LogThis();
	f6();
	specific.LogThis();
}
void f4()
{
	LogSectionTrigger currentStackTrigger2(LogSectionRepository::Instance(), "TEST");
	ThisSectionLogging otherSection("F1_PATH");
	std::cout << "In f4()" << std::endl;
	otherSection.LogThis();
	f5();
}
void f3()
{
	ThatSectionLogging critical("TEST");
	std::cout << "In f3()" << std::endl;
	critical.LogThis();
	f4();
	critical.LogThis();
}
void f2()
{
	LogSectionTrigger currentStackTrigger(LogSectionRepository::Instance(),"F2_PATH");
	LogSectionTrigger currentStackTrigger2(LogSectionRepository::Instance(), "TEST");
	std::cout << "In f2()" << std::endl;
	f3();
}
void f1()
{
	LogSectionTrigger stackTrigger(LogSectionRepository::Instance(), "F1_PATH");
	std::cout << "In f1()" << std::endl;
	f2();
}


int main()
{
	f4();
	f2();
	f3();
	f1();

	return 0;
}

